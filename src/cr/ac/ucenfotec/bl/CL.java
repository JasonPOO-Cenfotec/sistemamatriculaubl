package cr.ac.ucenfotec.bl;

import java.util.ArrayList;

public class CL {

    private ArrayList<Carrera> listaCarreras;
    private ArrayList<Curso> listaCursos;

    public CL(){
        listaCarreras = new ArrayList<>();
        listaCursos = new ArrayList<>();
    }

    public String agregarCarrera(String codigo, String nombre, boolean esAcreditada){
        Carrera carreraNueva = new Carrera(codigo,nombre,esAcreditada);
        listaCarreras.add(carreraNueva);

        return "La carrera fue registrada exitosamente!";
    }

    public ArrayList<String> listarCarreras(){
        ArrayList lista =new ArrayList();
        for (Carrera carreraTemp:listaCarreras) {
            lista.add(carreraTemp.toString());
        }
        return lista;
    }

    // Método que adiciona nuevos cursos al Array listaCursos.
    public String agregarCurso(String codigo, String nombre, int creditos, double costo){
        Curso cursoNuevo = new Curso(codigo, nombre, creditos, costo);
        listaCursos.add(cursoNuevo);

        return "El curso fue registrado exitosamente!";
    }

    public ArrayList<String> listarCursos(){
        ArrayList<String> listado = new ArrayList<>();
        for (Curso cursoTemp:listaCursos) {
            listado.add(cursoTemp.toString());
        }
        return listado;
    }

    public String asociarCursoACarrera(String codigoCarrera, String codigoCurso){
        // 1. Buscar si existe la carrera.
        // 2. Si no existe la carrera vamos a devolver un mensaje de error.
        // 3. Buscar si exise el curso.
        // 4. Si no existe el curso vamos a devolver un mensaje de error.
        // 5. Agregar a  la lista de curso de esa carrera, el curso indicado por el usuario.
        // 6. devolver el mesaje de que se asocio de manera correcta el curso a la carrera.
        Carrera carrera = buscarCarrera(codigoCarrera);
        if(carrera != null){
            Curso curso = buscarCurso(codigoCurso);
            if(curso != null){
                carrera.agregarCurso(curso);  // Aquí es donde voy hacer la agregación de curso a carrera
                return "El curso fue agregado a la carrera de manera exitosa!!";
            }else
                return "El curso ingresado no existe en el catálogo de la Univerdidad!";
        }else
            return "La carrera ingresada no existe en el catálogo de la Univerdidad!";
    }

    public Carrera buscarCarrera(String codigoCarrera){
        for(Carrera carreraTemp:listaCarreras) {
            if(carreraTemp.getCodigo().equals(codigoCarrera)){
                 return carreraTemp;
            }
        }
        return null;
    }

    public Curso buscarCurso(String codigoCurso){
        for (Curso cursoTemp:listaCursos) {
            if(cursoTemp.getCodigo().equals(codigoCurso)){
                return cursoTemp;
            }
        }
        return null;
    }

    public String registrarGrupo(String codigoCurso, int numeroGrupo, String codigoGrupo){
        // 1. Buscar si exise el curso.
        // 2. Si no existe el curso vamos a devolver un mensaje de error.
        // 3. validar si existe el grupo (Queda pendiente como practica)
        // 4. Le solicito al curso que cree el grupo.
        Curso curso = buscarCurso(codigoCurso);
        if(curso != null){
            curso.crearGrupo(numeroGrupo,codigoGrupo); // Composición
            return "El grupo para el curso " + curso.getNombre() + ", fue creado exitosamente!";
        }else
            return "El curso ingresado no existe en el catálogo de la Univerdidad!";
    }
}
